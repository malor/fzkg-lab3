CPP = g++
LIBS = -lGL -lGLU -lglut
CPP_FLAGS = -Wall -Werror -ansi -pedantic

default: main.o drawing.o
	$(CPP) main.o drawing.o -o main $(LIBS)

clean:
	rm *.o main

main.o: main.cpp
	$(CPP) -c main.cpp $(CPP_FLAGS) $(APP_FLAGS)

drawing.o: drawing.cpp
	$(CPP) -c drawing.cpp $(CPP_FLAGS) $(APP_FLAGS)
