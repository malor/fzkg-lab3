#include <cmath>

#include <GL/gl.h>

#include "drawing.h"

void drawCube()
{
	glBegin(GL_QUADS);
	
	// right plane
	glColor3f(1, 0, 0);
	glVertex3f(1, 1, -1);
	glVertex3f(1, 1, 1);
	glVertex3f(1, -1, 1);
	glVertex3f(1, -1, -1);

	// left plane
	glColor3f(0, 1, 0);
	glVertex3f(-1, 1, -1);
	glVertex3f(-1, 1, 1);
	glVertex3f(-1, -1, 1);
	glVertex3f(-1, -1, -1);

	// front plane
	glColor3f(0, 0, 1);
	glVertex3f(1, 1, 1);
	glVertex3f(-1, 1, 1);
	glVertex3f(-1, -1, 1);
	glVertex3f(1, -1, 1);

	// back plane
	glColor3f(1, 1, 0);
	glVertex3f(1, 1, -1);
	glVertex3f(-1, 1, -1);
	glVertex3f(-1, -1, -1);
	glVertex3f(1, -1, -1);

	// up plane
	glColor3f(1, 0, 1);
	glVertex3f(1, 1, -1);
	glVertex3f(-1, 1, -1);
	glVertex3f(-1, 1, 1);
	glVertex3f(1, 1, 1);

	// down plane
	glColor3f(0, 1, 1);
	glVertex3f(1, -1, -1);
	glVertex3f(-1, -1, -1);
	glVertex3f(-1, -1, 1);
	glVertex3f(1, -1, 1);

	glEnd();
}

void drawCylinder(int n)
{
	glBegin(GL_TRIANGLE_STRIP);
		glColor3f(1, 1, 0);
		for (int i=0; i<=n; i++)
		{
			glVertex3f(cos(360. / n * i * (M_PI / 180)), 1, -sin(360./n * i * (M_PI / 180)));
			glVertex3f(cos(360. / n * i * (M_PI / 180)), -1, -sin(360./n * i * (M_PI / 180)));
		}
	glEnd();

	glBegin(GL_TRIANGLE_FAN);
		glColor3f(1, 0, 0);
		glVertex3f(0, 1, 0);
		for (int i=0; i<=n; i++)
			glVertex3f(cos(360. / n * i * (M_PI / 180)), 1, -sin(360./n * i * (M_PI / 180)));
	glEnd();

	glBegin(GL_TRIANGLE_FAN);
		glColor3f(0, 0, 1);
		glVertex3f(0, -1, 0);
		for (int i=0; i<=n; i++)
			glVertex3f(cos(360. / n * i * (M_PI / 180)), -1, -sin(360./n * i * (M_PI / 180)));
	glEnd();
}

void drawCone(int n)
{
	glBegin(GL_TRIANGLE_FAN);
		glColor3f(1, 1, 0);
		glVertex3f(0, 1, 0);
		for (int i=0; i<=n; i++)
			glVertex3f(cos(360. / n * i * (M_PI / 180)), -1, -sin(360./n * i * (M_PI / 180)));
	glEnd();

	glBegin(GL_TRIANGLE_FAN);
		glColor3f(0, 0, 1);
		glVertex3f(0, -1, 0);
		for (int i=0; i<=n; i++)
			glVertex3f(cos(360. / n * i * (M_PI / 180)), -1, -sin(360./n * i * (M_PI / 180)));
	glEnd();
}

void drawRobot()
{
	// body
	glPushMatrix();
		glScalef(2, 3, 2);
		drawCylinder(40);
	glPopMatrix();
	
	// one foot
	glPushMatrix();
		glScalef(0.5, 3, 0.5);
		glTranslatef(2, -1, 0);
		drawCylinder(40);
	glPopMatrix();

	// another foot
	glPushMatrix();
		glScalef(0.5, 3, 0.5);
		glTranslatef(-2, -1, 0);
		drawCylinder(40);
	glPopMatrix();

	// one hand
	glPushMatrix();
		glRotatef(30, 0, 0, 1);	
		glScalef(0.5, 2, 0.5);
		glTranslatef(5.5, -1, 0);
		drawCylinder(40);
	glPopMatrix();

	// another hand
	glPushMatrix();
		glRotatef(-30, 0, 0, 1);	
		glScalef(0.5, 2, 0.5);
		glTranslatef(-5.5, -1, 0);
		drawCylinder(40);
	glPopMatrix();

	// head
	glPushMatrix();
		glTranslatef(0, 4, 0);
		drawCylinder(40);

		// one eye
		glPushMatrix();
			glScalef(0.25, 0.25, 0.25);
			glRotatef(90, 1, 0, 0);
			glTranslatef(2, 4, -1);
			drawCylinder(40);
		glPopMatrix();

		// another eye
		glPushMatrix();
			glScalef(0.25, 0.25, 0.25);
			glRotatef(90, 1, 0, 0);
			glTranslatef(-2, 4, -1);
			drawCylinder(40);
		glPopMatrix();

	glPopMatrix();
}
