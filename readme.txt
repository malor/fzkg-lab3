To build the project follow these steps:
    1. Go to the project's source dir.
    2. Do 'make clean'.
    3. Do 'APP_FLAGS="-DPROJECTION_TYPE -DPROJECTION_TYPE_USE_OPENGL" make'
       where PROJECTION_TYPE should be replaced with ORTHO, OBLIQUE or PERSPECTIVE,
       and PROJECTION_TYPE_USE_OPENGL should be replaced with ORTHO_OPENGL or PERSPECTIVE_OPENGL
       appropriately.
    4. In order to build the project with other compiler flags do 'make clean' first.
