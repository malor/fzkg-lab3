#ifndef __DRAWING_H__
#define __DRAWING_H__

void drawCube();
void drawCylinder(int n = 40);
void drawCone(int n = 40);

void drawRobot();

#endif /* __DRAWING_H__ */
