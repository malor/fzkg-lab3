#include <cmath>

#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>

#include "drawing.h"

GLfloat vertices[][3] = 
    {
        {-1.0, -1.0, -1.0}, { 1.0, -1.0, -1.0},
        { 1.0,  1.0, -1.0}, {-1.0,  1.0, -1.0},
        {-1.0, -1.0,  1.0}, { 1.0, -1.0,  1.0},
        { 1.0,  1.0,  1.0}, {-1.0,  1.0,  1.0}
    };

GLfloat color[][4] = 
    {
        {0, 0, 0, 0.51f}, {1, 0, 0, 0.51f},
        {1, 1, 0, 0.51f}, {0, 1, 0, 0.51f},
        {0, 0, 1, 0.51f}, {1, 0, 1, 0.51f},
        {1, 1, 1, 0.51f}, {0, 1, 1, 0.51f}
    };

void polygon(int a, int b, int c, int d)
{
	glBegin(GL_POLYGON);
		glColor3fv(color[a]);
		glVertex3fv(vertices[a]);
		glColor3fv(color[b]);
		glVertex3fv(vertices[b]);
		glColor3fv(color[c]);
		glVertex3fv(vertices[c]);
		glColor3fv(color[d]);
		glVertex3fv(vertices[d]);
	glEnd();
}

void ColorCube()
{
	polygon(0, 3, 2, 1);
	polygon(2, 3, 7, 6);
	polygon(0, 4, 7, 3);
	polygon(1, 2, 6, 5);
	polygon(4, 5, 6, 7);
	polygon(0, 1, 5, 4);
}

void idle()
{
    glutPostRedisplay();
}

void reshape(int x, int y)
{
	glViewport(0, 0, x, y);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

#ifdef ORTHO
    #ifdef ORTHO_OPENGL
        glOrtho(-2, 0, 0, 2, -2, 2);
    #else
        const float XMIN = -2;
        const float XMAX =  0;
        const float YMIN =  0;
        const float YMAX =  2;
        const float ZMIN = -2;
        const float ZMAX =  2;

        const float ortho[4][4] =
        {
            {2 / (XMAX - XMIN), 0, 0, 0},
            {0, 2 / (YMAX - YMIN), 0, 0},
            {0, 0, -2 / (ZMAX - ZMIN), 0},
            { -(XMAX + XMIN) / (XMAX - XMIN),
              -(YMAX + YMIN) / (YMAX - YMIN),
              -(ZMAX + ZMIN) / (ZMAX - ZMIN),
              1}
        };

        glLoadMatrixf(&ortho[0][0]);
    #endif
#endif

#ifdef OBLIQUE
	const float XMIN = -10;
	const float XMAX =  10;
	const float YMIN = -10;
	const float YMAX =  10;
	const float ZMIN = -10;
	const float ZMAX =  10;

	const float THETA = 5 * M_PI / 180;
	const float PHI = 60 * M_PI / 180;

	const float oblique[4][4] =
	{
		{2 / (XMAX - XMIN), 0, 0, 0},
		{0, 2 / (YMAX - YMIN), 0, 0},
		{  2 * (1 / tan(THETA)) / (XMAX - XMIN),
           2 * (1 / tan(PHI)) / (YMAX - YMIN),
          -2 / (ZMAX - ZMIN), 0},
		{ -(XMAX + XMIN) / (XMAX - XMIN),
          -(YMAX + YMIN) / (YMAX - YMIN),
          -(ZMAX + ZMIN) / (ZMAX - ZMIN),
          1}
	};

	glLoadMatrixf(&oblique[0][0]);
#endif

#ifdef PERSPECTIVE
	const float XMIN = -5;
	const float XMAX =  2;
	const float YMIN = -2;
	const float YMAX =  3;
	const float ZMIN =  6;
	const float ZMAX = 15;

    #ifdef PERSPECTIVE_OPENGL
        glFrustum(XMIN, XMAX, YMIN, YMAX, ZMIN, ZMAX);
    #else
        const float perspective[4][4] =
        {
            {2 * ZMIN / (XMAX - XMIN), 0, 0, 0},
            {0, 2 * ZMIN / (YMAX - YMIN), 0, 0},
            { (XMAX + XMIN) / (XMAX - XMIN),
              (YMAX + YMIN) / (YMAX - YMIN),
              -(ZMAX + ZMIN) / (ZMAX - ZMIN),
              -1},
            {0, 0, -2 * ZMAX*ZMIN / (ZMAX - ZMIN), 0}
        };

        glLoadMatrixf(&perspective[0][0]);
    #endif
#endif

	glMatrixMode(GL_MODELVIEW);
}

void display()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);

	glLoadIdentity();

#ifdef PERSPECTIVE
	gluLookAt(0, -5, -10, 0, 0, 0, 0, 1, 0);
#endif

	drawRobot();

	glFlush();
	glutSwapBuffers();
}

int main(int argc, char *argv[])
{
    glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH);

    glutInitWindowSize(800, 600);
    glutInitWindowPosition(200, 200);
	glutCreateWindow("OpenGL");

	glutReshapeFunc(reshape);
	glutIdleFunc(idle);
	glutDisplayFunc(display);
	
	glEnable(GL_ALPHA_TEST);
	glAlphaFunc(GL_GREATER, 0);
	glEnable(GL_DEPTH_TEST);

	glLineWidth(15);	
	glPointSize(15);

	glClearColor(0.4f, 0.4f, 0.4f, 0);
	
	glutMainLoop();		
    return 0;
}
